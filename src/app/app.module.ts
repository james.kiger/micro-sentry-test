import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MicroSentryModule } from '@micro-sentry/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MicroSentryModule.forRoot({
      dsn: 'https://kj12kj1n23@sentry.domain.com/123',
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
